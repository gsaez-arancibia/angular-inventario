import { Component, ViewChild, OnInit, OnDestroy } from '@angular/core';
import { MarcasService } from '../../services/marcas.service';
import { Tabla } from '../../clases/Tabla';
import { Marca } from '../../interfaces/interfaces';

import { FormGroup, FormControl, Validators } from '@angular/forms';
import { DataTableDirective } from 'angular-datatables';

import Swal from 'sweetalert2';

// Para usar jQuery
declare var $: any;

@Component({
  selector: 'app-marca',
  templateUrl: './marca.component.html',
  styles: []
})
export class MarcaComponent implements OnInit, OnDestroy {
	@ViewChild(DataTableDirective, {static: false}) dtElement: DataTableDirective;

	marcas: Marca[] = [];
	tabla = new Tabla();
	forma: FormGroup;
	accion: string;
	marcaEditar: Marca;

	constructor(private _marcasService: MarcasService){
		this._marcasService.getAll()
			.subscribe( (marcas: Marca[]) => {
				this.marcas = marcas;
				this.tabla.dtTrigger.next();
			});
	}

	ngOnInit() {
		this.buildForm();
	}

	ngOnDestroy() {
	    // Do not forget to unsubscribe the event
	    this.tabla.dtTrigger.unsubscribe();
  	}

  	agregarMarca(){
  		// Guardar datos
  		var nombre = this.forma.value.nombre.toUpperCase();
  		
  		this._marcasService.agregarMarca(nombre).subscribe( (resp: any) => {
  			if(resp.ok){
  				// Agregar a la lista de marcas
  				this.marcas.push({ id: resp.id, nombre: nombre });

  				// Ordenar marcas por nombre en asc 
  				this.marcas.sort((unaMarca, otraMarca) => unaMarca.nombre.localeCompare(otraMarca.nombre));

  				// Ocultar modal
  				$('#modalMarca').modal('hide');

  				// Renderizar tabla
  				this.tabla.rerender(this.dtElement);

  				// Mostrar alerta
  				Swal.fire('', `La marca ${ nombre } ha sido agregada correctamente.`, 'success');

  				// Reset formulario
  				this.resetForm();
  			} else {
  				// Ocultar modal
  				$('#modalMarca').modal('hide');

  				// Mostrar alerta
  				Swal.fire('Oops!', `Ha ocurrido un error al agregar la marca ${ nombre }. Es posible que la marca ya exista.`, 'error');

  				// Reset formulario
  				this.resetForm();
  			}
  		});
  	}

  	editarMarca(){
  		// Guardar datos
  		var nombre = this.forma.value.nombre.toUpperCase();

  		this._marcasService.editarMarca(this.marcaEditar.id, nombre).subscribe( (resp: any) => {
  			if(resp.ok){
  				// Actualizar nombre en lista de marcas 
  				const index = this.marcas.findIndex(marca => marca.nombre == this.marcaEditar.nombre);
  				this.marcas[index].nombre = nombre;

  				// Ordenar marcas por nombre en asc 
  				this.marcas.sort((unaMarca, otraMarca) => unaMarca.nombre.localeCompare(otraMarca.nombre));

  				// Ocultar modal
  				$('#modalMarca').modal('hide');

  				// Renderizar tabla
  				this.tabla.rerender(this.dtElement);

  				// Mostrar alerta
  				Swal.fire('', `La marca ha sido editada correctamente.`, 'success');

  				// Reset formulario
  				this.resetForm();
  			} else {
  				// Ocultar modal
  				$('#modalMarca').modal('hide');

  				// Mostrar alerta
  				Swal.fire('Oops!', `Ha ocurrido un error al editar la marca. Es posible que el nombre ya exista.`, 'error');

  				// Reset formulario
  				this.resetForm();
  			}
  		});
  	}

  	eliminarMarca(marca: Marca, i: number){
  		this._marcasService.eliminarMarca(marca.id).subscribe( (resp: any) => {
  			if(resp.ok){
  				// Eliminar de la lista de marcas
  				this.marcas.splice(i, 1);

  				// Renderizar tabla
  				this.tabla.rerender(this.dtElement);

  				// Mostrar alerta
  				Swal.fire('', `La marca ${ marca.nombre } ha sido eliminada correctamente.`, 'success');
  			} else {
  				Swal.fire('Oops!', `No es posible eliminar la marca ${ marca.nombre } mientras se encuentre asociada a un producto.`, 'error');
  			}
  		});
  	}

  	onSubmit(){
  		if(this.accion == 'agregar'){
  			this.agregarMarca();
  		} else {
  			this.editarMarca();
  		}
  	}

  	showModal(marca?: Marca){
  		if(marca){
  			this.accion = 'editar';
  			this.forma.controls['nombre'].setValue(marca.nombre);
  			this.marcaEditar = marca;
  		} else {
  			this.accion = 'agregar';
  		}

  		// Mostrar modal
  		$('#modalMarca').modal('show');
  	}

  	private buildForm(){
		this.forma = new FormGroup({
			'nombre': new FormControl('', Validators.required)
		});
  	}

  	resetForm(){
        this.forma.reset();
  	}
}
