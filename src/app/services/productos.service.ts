import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ProductosService {
	url: string = 'http://localhost:8000';

	constructor(private http: HttpClient){

	}

	getAll(){
		return this.http.get(`${ this.url }/api/productos`);
	}

	agregarProducto(producto){
		return this.http.post(`${ this.url }/api/productos`, producto);
	}

	editarProducto(id: number, producto: any){
		return this.http.put(`${ this.url }/api/productos/${ id }`, producto);
	}

	eliminarProducto(id: number){
		return this.http.delete(`${ this.url }/api/productos/${ id }`);
	}
}
