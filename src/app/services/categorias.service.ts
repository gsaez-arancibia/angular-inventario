import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class CategoriasService {
	url: string = 'http://localhost:8000';

	constructor(private http: HttpClient){ 

	}

	getAll(){
		return this.http.get(`${ this.url }/api/categorias`);
	}

	agregarCategoria(nombre: string){
		return this.http.post(`${ this.url }/api/categorias`, {nombre: nombre});
	}

	editarCategoria(id:number, nombre: string){
		return this.http.put(`${ this.url }/api/categorias/${ id }`, {nombre: nombre});
	}

	eliminarCategoria(id: number){
		return this.http.delete(`${ this.url }/api/categorias/${ id }`);
	}
}
