# Inventario (Frontend) - Angular

### Pre-requisitos 📋

* Angular
* Node.js - npm

### Instalación 🔧

_En el directorio del proyecto ingresar el siguiente comando a través de una terminal_

* npm install

## Despliegue 📦

_Ejecutar el siguiente comando para correr el proyecto_

* ng serve

## Inventario (Backend) - Laravel 🛠️

* [Ir al repositorio](https://gitlab.com/seein/laravel-inventario)